const fs =require('fs')

function writeToFile(arr, ind) {
  const arg = arr[ind]

  if (arg) {
    fs.appendFile('toWriteFileName.txt', arr.length === ind + 1 ? arg : `${arg} `, (err) => {
      if (err) {
        console.log("Error adding", arg)
        throw err
      }
      else {
        console.log("The symbols", arg, "have been added")
        writeToFile(arr, ++ind)
      }
    })

  }
  else return  
}

writeToFile(process.argv, 3)